package com.sparkkafkaintegration.spark_kafka;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class KafkaReceiver {
    private static String KafkaBrokerEndpoint = "localhost:9092";
    private static String KafkaTopic = "my-topic";
    private static String KafkaGroup = "my-topic_group";

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", KafkaBrokerEndpoint);
        properties.setProperty("group.id", KafkaGroup);
        properties.setProperty("auto.commit.interval.ms", "1000");
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);) {
            consumer.subscribe(Arrays.asList(KafkaTopic));

            for (int i = 0; i < 100; i++) {
                ConsumerRecords<String, String> records = consumer.poll(1000L);
                System.out.println("Size: " + records.count());
                for (ConsumerRecord<String, String> record : records) {
                    System.out.println("Message : " + record.value());
                }
            }

        }
    }
}
