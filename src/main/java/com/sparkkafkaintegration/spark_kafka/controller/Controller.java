package com.sparkkafkaintegration.spark_kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sparkkafkaintegration.spark_kafka.model.MessageModel;
import com.sparkkafkaintegration.spark_kafka.service.myService;

@RestController
public class Controller {
    @Autowired
    myService service;

    @PostMapping("/send")
    ResponseEntity<?> sender(@RequestBody MessageModel m) {
        System.out.println(m.getMessage());
        return ResponseEntity.status(HttpStatus.OK).body(service.messageSender(m.getMessage()));
    }

    @GetMapping("/receive")
    ResponseEntity<?> receiver() {
        return ResponseEntity.status(HttpStatus.OK).body(service.messageReceiver());
    }

}
