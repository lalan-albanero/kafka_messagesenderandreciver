package com.sparkkafkaintegration.spark_kafka;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class kafkaProducer {

    private static String KafkaBrokerEndpoint = "localhost:9092";
    private static String KafkaTopic = "my-topic";
    // private static String CsvFile = "C:\\Users\\Lalan
    // kumar\\Desktop\\Spark\\sparkapp\\src\\main\\resources\\static\\SampleCSVFile_11kb.csv";
    Properties properties = new Properties();

    public static void main(String[] args) throws  InterruptedException {

        kafkaProducer kafkaProducer = new kafkaProducer();
        kafkaProducer.PublishMessages();
        System.out.println("Producing job completed");
    }

    private void PublishMessages() throws InterruptedException {
        properties.setProperty("bootstrap.servers", KafkaBrokerEndpoint);
        properties.setProperty("kafka.topic.name", KafkaTopic);

        KafkaProducer<String, byte[]> kafkaProducer= new KafkaProducer<String,byte[]>(this.properties ,new StringSerializer(),new ByteArraySerializer());

        for(int i=0;i<100;i++){
            byte[] payload=(i+"lalan"+new Date()).getBytes();

            System.out.println(i+"lalan"+new Date());
            ProducerRecord<String,byte[]> record=new ProducerRecord<String,byte[]>(KafkaTopic, payload);
            kafkaProducer.send(record);
            Thread.sleep(1000);
        }
        
    }

}
