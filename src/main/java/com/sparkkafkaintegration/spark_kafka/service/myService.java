package com.sparkkafkaintegration.spark_kafka.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

@Service
public class myService {
    Properties properties = new Properties();
    private static String KafkaBrokerEndpoint = "localhost:9092";
    private static String KafkaTopic = "my-topic";
    private static String KafkaGroup = "my-topic_group";

    public String messageSender(String message) {
        properties.setProperty("bootstrap.servers", KafkaBrokerEndpoint);
        properties.setProperty("kafka.topic.name", KafkaTopic);

        KafkaProducer<String, byte[]> kafkaProducer = new KafkaProducer<String, byte[]>(this.properties,
                new StringSerializer(), new ByteArraySerializer());
        byte[] payload = (message).getBytes();
        ProducerRecord<String, byte[]> record = new ProducerRecord<String, byte[]>(KafkaTopic, payload);
        kafkaProducer.send(record);

        return "Success";
    }

    public List<String> messageReceiver() {
        properties.setProperty("bootstrap.servers", KafkaBrokerEndpoint);
        properties.setProperty("group.id", KafkaGroup);
        properties.setProperty("auto.commit.interval.ms", "1000");
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        List<String> messageList = new ArrayList<>();

        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);) {
            consumer.subscribe(Arrays.asList(KafkaTopic));

            ConsumerRecords<String, String> records = consumer.poll(10000L);
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Message : " + record.value());
                messageList.add(record.value());
            }

        }
        return messageList;
        // return "Success";
    }
}
